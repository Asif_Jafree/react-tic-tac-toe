import React from 'react';

const initialArray = Array(9).fill('');
class TicTacToe extends React.Component {
  constructor() {
    super();
    this.state = {
      first: initialArray,
      turn: 'X',
      winner: '',
      text: 'X play First',
      count: 0,
    };
  }

  winner = (squares) => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }
    return null;
  };

  restart = () => {
    initialArray.fill('');
    this.setState({
      first: initialArray,
      turn: 'X',
      winner: '',
      text: 'X play First',
      count: 0,
    });
  };

  onchange(index) {
    const { turn, winner, first, count } = this.state;

    if (winner) {
      return;
    }

    // console.log(this.state.count);

    if (turn === 'X' && winner === '' && initialArray[index] === '') {
      initialArray[index] = 'X';
      this.setState({
        first: initialArray,
        turn: 'O',
        text: 'O turn to play',
        count: count + 1,
      });

      let win = this.winner(first);
      console.log(this.state.count);
      if (win) {
        this.setState({
          winner: win,
          text: `${win} win the game`,
        });
      } else if (count === 8) {
        this.setState({
          text: 'Game Draw !!!',
        });
      }
    } else if (initialArray[index] === '') {
      initialArray[index] = 'O';
      this.setState({
        first: initialArray,
        turn: 'X',
        text: 'X turn to play',
        count: count + 1,
      });
      let win = this.winner(first);

      if (win) {
        this.setState({
          winner: win,
          text: `${win} win the game`,
        });
      }
    }
  }
  render() {
    const { first } = this.state;
    return (
      <div className="tic-tac-toe">
        <div className="container">
          <div className="first-row">
            <div className="box left-col top" onClick={() => this.onchange(0)}>
              {first[0]}
            </div>
            <div className="box top" onClick={() => this.onchange(1)}>
              {first[1]}
            </div>
            <div
              className="box right-col top "
              onClick={() => this.onchange(2)}
            >
              {first[2]}
            </div>
          </div>
          <div className="second-row">
            <div className="box left-col" onClick={() => this.onchange(3)}>
              {first[3]}
            </div>
            <div className="box" onClick={() => this.onchange(4)}>
              {first[4]}
            </div>
            <div className="box right-col" onClick={() => this.onchange(5)}>
              {first[5]}
            </div>
          </div>
          <div className="third-row">
            <div
              className="box left-col bottom"
              onClick={() => this.onchange(6)}
            >
              {first[6]}
            </div>
            <div className="box bottom" onClick={() => this.onchange(7)}>
              {first[7]}
            </div>
            <div
              className="box right-col bottom"
              onClick={() => this.onchange(8)}
            >
              {first[8]}
            </div>
          </div>
          <div className="text">{this.state.text}</div>
          <button onClick={() => this.restart()}>Restart the Game</button>
        </div>
      </div>
    );
  }
}

export default TicTacToe;
